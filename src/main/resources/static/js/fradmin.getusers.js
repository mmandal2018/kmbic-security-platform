function getUserList() {
    $.getJSON('api/getUsers', function (data) {
        var sdata = data;

        var userTable = $('#userTable').DataTable({
            'responsive': true,
            'bLengthChange': false,
            "pageLength": 10,
            "scrollCollapse": true,
            "paging": true,
            'data': sdata,
            'columns': [
                {"data": "registrationTime"},
                {"data": "person_id"},
                {"data": "employeeId"},
                {"data": "name"},
                {"data": "group"},
                {"data": "thumbnail"},
                {
                    data: null,
                    className: "center",
                    defaultContent: '<a class="editor_remove" href="#"><i class="fa fa-trash" style="font-size:18px"></i> </a>'
                }

            ],
            'order': [[0, 'asc']],
            'columnDefs': [

                {'render': function (data, type, row) {
                        return getEventTime(data);
                    }, 'targets': 0},
                {'render': function (data, type, row) {
                        return showProfileImage(data);
                    }, 'targets': 5}
            ]
        });



        $('.editor_remove').click(function () {
            var fields = data[$(this).closest('tr').index()];
            console.log(fields.person_id);
            $('#delUserVal').text(fields.name);

            $('#deleteUserDialog').dialog({
                title: 'Delete User',
                resizable: false,
                height: "auto",
                width: 450,
                modal: true,
                buttons: {
                    "Delete": function () {
                        deleteUser(fields.person_id);
                    }
                }
            });
        })




    });
}

function getEventTime(regTime) {
    var timeStr = "";
    if (regTime !== "")
        var timeStr = moment(regTime).format("DD MMM YYYY, hh:mm:ss A");

    return timeStr;
}


function showProfileImage(imageStr) {
    var imageUrl = "";
    var imgElement = "";
    imgElement = '<div class= "img-circle">'
    imageUrl = '<img class="img-circle" src="data:image/jpeg;base64,';
    imgElement = imgElement + imageUrl + imageStr + '"></div>';
    return imgElement;

}



var previewImg = function (event) {
    var preview = document.getElementById('previewImage');
    preview.src = URL.createObjectURL(event.target.files[0]);
}



function deleteUser(personId) {


    $.ajax({
        url: "api/delUser",
        data: personId,
        type: 'POST',
        contentType: 'text/plain',
        success: function (data, textStatus, jqXHR) {

            $("#deleteUserDialog").dialog('close');
            if (data.errorCode === 0) {
                $("#userTableFeedback").html('<div class="alert alert-success offset4 span4"><button type="button" class="close">×</button>' + data.errorMsg + '</div>');
                $('.alert .close').on("click", function (e) {
                    $(this).parent().fadeTo(500, 0).slideUp(500);
                });
            } else {
                $("#userTableFeedback").html('<div class="alert alert-warning offset4 span4"><button type="button" class="close">×</button>' + data.errorMsg + '</div>');
                $('.alert .close').on("click", function (e) {
                    $(this).parent().fadeTo(500, 0).slideUp(500);
                });
            }
            //getSavedCameras();

        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR.responseText);

            $("#deleteUserDialog").dialog('close');
            $("#userTableFeedback").html('<div class="alert alert-warning offset4 span4"><button type="button" class="close">×</button>' + jqXHR.responseText + '</div>');
            $('.alert .close').on("click", function (e) {
                $(this).parent().fadeTo(500, 0).slideUp(500);
            });
        }
    });



}