var previewImg = function (event) {
    var preview = document.getElementById('previewImage');
    preview.src = URL.createObjectURL(event.target.files[0]);
}


$("#addUserform").validate({
    submitHandler: function (form) {
        $('.form-process').fadeIn();


        if ($('#imgFile').get(0).files.length === 0) {
            $("#userStatus").html('<div class="alert alert-danger offset4 span4"><button type="button" class="close">×</button>' + "No Image File Selected" + '</div>');
            $('.alert .close').on("click", function (e) {
                $(this).parent().fadeTo(500, 0).slideUp(500);
            });
            return;
        }

        var files = document.getElementById('imgFile').files;
        var base64Image = getBase64andSubmit(files[0]);
    }

});


function getBase64andSubmit(file) {
    var reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = function () {
        var base64Image = reader.result;
        base64Image = base64Image.replace("data:image/jpeg;base64,", "");
        var photoList = new Array();
        photoList.push(base64Image);

        var userInfo = {
            registrationTime: new Date(),
            firstName: $("#firstName").val(),
            lastName: $("#lastName").val(),
            group: $("#groupName").val(),
            employeeId: $("#employeeId").val(),
            phoneNum: $("#phoneNum").val(),
            photoList: photoList
        };

        $.ajax({
            url: "api/addUser",
            data: JSON.stringify(userInfo),
            type: 'POST',
            contentType: 'application/json',
            success: function (data, textStatus, jqXHR) {
                if (data.errorCode === 0) {
                    $("#userStatus").html('<div class="alert alert-success offset4 span4"><button type="button" class="close">×</button>' + data.errorMsg + '</div>');
                    $('.alert .close').on("click", function (e) {
                        $(this).parent().fadeTo(500, 0).slideUp(500);
                    });
                } else {
                    $("#userStatus").html('<div class="alert alert-danger offset4 span4"><button type="button" class="close">×</button>' + data.errorMsg + '</div>');
                    $('.alert .close').on("click", function (e) {
                        $(this).parent().fadeTo(500, 0).slideUp(500);
                    });
                }
                resetAddUserForm();

            },
            error: function (jqXHR, textStatus, errorThrown) {

                $("#userStatus").html('<div class="alert alert-danger offset4 span4"><button type="button" class="close">×</button>' + jqXHR.responseText + '</div>');
                $('.alert .close').on("click", function (e) {
                    $(this).parent().fadeTo(500, 0).slideUp(500);
                });
                resetAddUserForm();
            }

        });
    };
    reader.onerror = function (error) {
        $("#userStatus").html('<div class="alert alert-danger offset4 span4"><button type="button" class="close">×</button>' + error + '</div>');
        $('.alert .close').on("click", function (e) {
            $(this).parent().fadeTo(500, 0).slideUp(500);
        });
    };
}

function resetAddUserForm() {
    $("#firstName").val("");
    $("#lastName").val("");
    $("#groupName").val("");
    $("#employeeId").val("");
    $("#phoneNum").val("");
    document.getElementById("imgFile").value = "";
    var preview = document.getElementById('previewImage');
    preview.src = "";
}