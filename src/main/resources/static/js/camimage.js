/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var filesystem__version = "MX-V5.0.0.133";
var homepage__language = "en";
var windowname_suffix = "10__22__27__183";
var windowname_home = "mxHome10__22__27__183";
var windowname_ext = "mxExtern10__22__27__183";
var windowname_help = "mxHelp10__22__27__183";
var windowname_info = "mxInfo10__22__27__183";
var cam_address;

var pic_url;
var noframepath;
var n = Math.floor(Math.random() * 1000000);
var session_id = Math.floor(Math.random() * 1000000);
var startn = n;
var GetItOn = 1;
var framerate = 1;
var buffer = new Image();
var aktiv = null;
var passiv = null;
var preview_mode = "off";


$('document').ready(function () {
    getCameraSettingsInfo();
    animation_init();
});



function getCameraSettingsInfo() {
    $.getJSON('api/getCameraSettings', function (data) {
        var sdata = data;
        if (sdata !== null) {
            cam_address = 'http://' + sdata.vidcamip;
            pic_url = cam_address + "/record/current.jpg";
            noframepath = cam_address + "/decor/err_no_images.jpg";
            $('#camera1heading').html('<h3 class="box-title"><i class="fa fa-video-camera"></i> <span> Live Feed Cam 1 - ' + sdata.vidcamid + ' IP Address - ' + sdata.vidcamip + ' </span></h3>')
        }
    });
}

function pauseFeed() {
    document.CamPic.src = noframepath;
    GetItOn = 0;
    buffer.onload = null;
}

function resumeFeed() {
    GetItOn = 1;
    animation_init();
}

function openExtNoDeko(path, winname) {
    if (path.search(/^!/) >= 0) {
        path = path.replace(/^!/, "");
        path = path + "?cachedummy=" + Math.floor(Math.random() * 1000000);
    }
    return window.open(path, winname, "scrollbars=yes,resizable=yes,width=722,height=700");
}

function goUserlevel()
{
    if (self.name == windowname_ext)
    {
        if (window.opener == null || window.opener.closed)
            // Kein Opener oder bereits geschlossen
            window.open("/control/userimage.html", windowname_home);
        else
            window.opener.focus();
        self.window.close();
        return;
    }
    location.href = "/control/userimage.html";
}

function open_camerainfo(prefix, evt) {
    var text = "";
    if (evt
            && (evt.shiftKey || evt.altKey || evt.CtrlKey)) {
        text = "&text";
    }
    if (typeof prefix == "undefined")
        var prefix = "";
    var wdh = window.open(prefix + "/control/camerainfo?rand=" + Math.random() + text, windowname_info,
            "width=370,height=320,scrollbars=yes,resizable=yes");
    if (wdh)
        wdh.focus();
}

self.name = "mxHome10__22__27__183";
hostname = window.location.host;
pos_of_colon = hostname.indexOf(':');
if (pos_of_colon >= 0) {
    hostname = hostname.substring(0, pos_of_colon);
}

window.Navigation = (function () {
    var urls = ["/control/userimage.html", "/control/player", "/control/multiview"];
    function addParamsToURL(URL, params) {
        if (URL.substr(-1, 1) == "&") {
            ;
        } else if (URL.indexOf("?") < 0) {
            URL += "?";
        } else {
            URL += "&";
        }
        return (URL + params);
    }
    var obj = {
        switchToLive: function () {
            location.href = urls[0];
        },
        switchToPlayer: function () {
            location.href = urls[1];
        },
        switchToMultiview: function (additional_parameters) {
            location.href = addParamsToURL(urls[2], additional_parameters);
        }
    };
    return obj;
})();


function DoComplete()
{
    //debugmsg("DoComplete() buffer.src="+buffer.src);
    document.CamPic.src = buffer.src;
    GetItOn = 1;
    if (current_refresh_method_nr == 1 && framerate > 0) {// Sync onload implementieren
        if (passiv == null)
            Animation();

    }
}
function LoadError()
{
    document.CamPic.src = noframepath;
    GetItOn = 1;
    if (current_refresh_method_nr == 1 && framerate > 0) // Sync onload Fehlerbehandlung
        passiv = setTimeout("passiv=null;Animation();", 1000);
}

max_refresh_rate = 25;
function getframerate() {

    return 10;
}
function setframerate(fps) {
    var obj = document.dkdk.recordmult;
    // off == -99
    if (fps == -99) {
        obj.selectedIndex = 0;
        onframeratechange();
        return true;
    }
    // remaining options sorted in descending order.
    for (var nr = 1; nr < obj.options.length; nr++) {
        //debugmsg(nr + ':' + obj.options[nr].value);
        var optionValue = Math.floor(obj.options[nr].value);
        if (fps >= optionValue && optionValue > 0) {
            if (obj.selectedIndex != nr) {
                obj.selectedIndex = nr;
                onframeratechange();
            }
            return true;
        }
    }
    return false;
}

function onframeratechange() {

    framerate = getframerate();

    //Spreview_mode = get_preview_mode();

    //debugmsg("onframeratechange: aktiv=" + aktiv + " rm=" + current_refresh_method_nr + " fps=" + framerate + " preview_quality=" + preview_mode);
    if (aktiv) {
        window.clearInterval(aktiv);
        aktiv = null;
    }
    document.CamPic.onerror = null;
    buffer.onerror = null;
    if (framerate < 0) {
        if (current_refresh_method_nr == 2) { //  Stream ausschalten
            GetItOn = 1;
            Animation();
        }
        return;
    }
    // Refresh Typen
    if (current_refresh_method_nr == 2) { //  Stream anschalten
        buffer.onload = null;
        buffer.onerror = null;
        document.CamPic.onload = null;
        document.CamPic.onerror = function () {
            document.CamPic.onerror = null; // Workaround FF bug #733553
            document.CamPic.src = "";
            setTimeout("onframeratechange();", 500);
        };
        var imageURL = cam_adress + "/control/faststream.jpg?stream=full";
        if ("on" == preview_mode)
            imageURL += "&preview";
        imageURL += "&fps=" + framerate + "&rand=" + String(n++);
        document.CamPic.src = imageURL;

        return;
    } else if (current_refresh_method_nr == 1) { // OnLoad synced
        session_id++;
        Animation();
        return;
    }
    aktiv = window.setInterval("Animation()", 1000 / framerate);
    GetItOn = 1;	// auf alle Faelle anzeigen!
    Animation(); // und go...
}

function Animation()
{
    var my_rm_nr = current_refresh_method_nr;
    var my_fr = framerate;
    var my_session = session_id;

    if (GetItOn == 1 || (my_rm_nr == 1 && my_fr > 0))
            // Bild ist geladen und das naechste kann
            { // angefordert werden.
                GetItOn = 0;
                var imageURL = pic_url + "?";



                if (my_rm_nr == 0)
                    imageURL += "rand=" + String(n++);
                else
                    imageURL += "sync=" + my_fr + "&session=" + my_session + "&rand=" + String(n++);

                document.CamPic.onerror = document.CamPic.onload = null
                buffer.onerror = LoadError;
                buffer.onload = DoComplete;
                buffer.src = imageURL;
            }
}
function animation_init() {
    onframeratechange();
}

current_refresh_method_nr = 0;
refreshmethods_type = new Array(0, 0, 0, 1, 2);
function switchRefreshMethod(new_value, fps) {
    if (refreshmethods_type[new_value] != refreshmethods_type[current_refresh_method_nr]) {

        var urlparts = location.href.split("?");
        var url = urlparts[0] + "?REFRESHMETHOD=" + new_value;
        if (typeof fps != "undefined" && fps > 0)
            url += "&REFRESHRATE=" + fps;
        location.href = url;


    } else {
        current_refresh_method_nr = new_value;
        if (typeof fps != "undefined" && fps > 0)
            setframerate(fps);
        onframeratechange();
    }

}
function reloadPage(new_fps) {
    var urlparts = location.href.split("?");
    var url = urlparts[0] + "?REFRESHMETHOD=" + current_refresh_method_nr;
    if (typeof new_fps == "undefined")
        new_fps = getframerate();
    url += "&REFRESHRATE=" + new_fps;
    location.href = url;
}

function can_switch_to_activex() {
    return 0;
}

self.focus();