
$('document').ready(function () {
    getServerSettingsInfo();
});


function getServerSettingsInfo() {
    $.getJSON('api/serverSetting', function (data) {
        var sdata = data.context.entity;
        if (sdata !== null) {
            alert(" inside: " + sdata.serverendpoint);
            $('#serverIP').val(sdata.serverip)
            $("#serverPort").val(sdata.serverport);
            $("#serverEndpoint").val(sdata.serverendpoint);
            $("#clientDir").val(sdata.deffsclientdir);
        }
    });
}


$("#serversettingsform").validate({
    submitHandler: function (form) {
        $('.form-process').fadeIn();

        var settingsInfo = {
            updateTime: new Date(),
            name: "admin@test.com",
            serverip: $("#serverIP").val(),
            serverport: $("#serverPort").val(),
            serverendpoint: $("#serverEndpoint").val(),
            deffsclientdir: $("#clientDir").val()
        };

        $.ajax({
            url: "api/saveServerSettings",
            data: JSON.stringify(settingsInfo),
            type: 'POST',
            contentType: 'application/json',
            success: function (data, textStatus, jqXHR) {
                //document.getElementById("taxqueryError").innerHTML = jqXHR.responseText;
                //showLogin();
                //$('#settingsFeedback').html('<h5 class="success-msg" style:"align-center>' + jqXHR.responseText + '<h5>');

                $("#settingsFeedback").html('<div class="alert alert-success offset4 span4"><button type="button" class="close">×</button>' + jqXHR.responseText + '</div>');
                $('.alert .close').on("click", function (e) {
                    $(this).parent().fadeTo(500, 0).slideUp(500);
                });
                //getSettingsInfo();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('error : ' + errorThrown);
                console.log('textStatus: ' + textStatus);
                console.log('jqXHR: ' + jqXHR);
                console.log(jqXHR.responseText);
                //document.getElementById("taxqueryError").innerHTML = jqXHR.responseText;
                //$('#settingsFeedback').html('<h5 class="error-msg" style:"align-center">' + jqXHR.responseText + '<h5>');
                $("#settingsFeedback").html('<div class="alert alert-warning offset4 span4"><button type="button" class="close">×</button>' + jqXHR.responseText + '</div>');
                $('.alert .close').on("click", function (e) {
                    $(this).parent().fadeTo(500, 0).slideUp(500);
                });

            }
        });
    }

});


$("#startFSClient").click(function () {
        $.ajax({
            url: "api/startFSClient",
            type: 'GET',
            success: function (data, textStatus, jqXHR) {
                $("#settingsFeedback").html('<div class="alert alert-success offset4 span4"><button type="button" class="close">×</button>' + jqXHR.responseText + '</div>');
                $('.alert .close').on("click", function (e) {
                    $(this).parent().fadeTo(500, 0).slideUp(500);
                });
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('error : ' + errorThrown);
                console.log('textStatus: ' + textStatus);
                console.log('jqXHR: ' + jqXHR);
                console.log(jqXHR.responseText);
                $("#settingsFeedback").html('<div class="alert alert-warning offset4 span4"><button type="button" class="close">×</button>' + jqXHR.responseText + '</div>');
                $('.alert .close').on("click", function (e) {
                    $(this).parent().fadeTo(500, 0).slideUp(500);
                });

            }
        });
    }
);

$("#stopFSClient").click(function () {
        alert('button clicked');
    }
);