var protocol;
var cameraTable;



$('document').ready(function () {
    getSavedCameras();
    protocol = $("#vidcamProtocol option:selected").text();

});


$('#vidcamProtocol').change(function () {
    protocol = $(this).find("option:selected").text();
});


$("#camerasettingsform").validate({
    submitHandler: function (form) {
        $('.form-process').fadeIn();
        var settingsInfo = {
            vidcamid: $("#vidcamID").val(),
            vidcamip: $("#vidcamIP").val(),
            vidcamport: $("#vidcamPort").val(),
            vidcamprotocol: protocol,
            vidcamstreampath: $("#vidcamPath").val(),
            vidcamuserid: $("#vidcamUsername").val(),
            vidcampwd: $("#vidcamPassword").val()
        };
        $.ajax({
            url: "api/saveCameraInfo",
            data: JSON.stringify(settingsInfo),
            type: 'POST',
            contentType: 'application/json',
            success: function (data, textStatus, jqXHR) {
                $('#modaladdcamera').modal('hide');

                $("#cameraSettingsFeedback").html('<div class="alert alert-success offset4 span4"><button type="button" class="close">×</button>' + jqXHR.responseText + '</div>');
                $('.alert .close').on("click", function (e) {
                    $(this).parent().fadeTo(500, 0).slideUp(500);
                });
                getSavedCameras();

            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR.responseText);
                $('#modaladdcamera').modal('hide');
                $("#cameraSettingsFeedback").html('<div class="alert alert-warning offset4 span4"><button type="button" class="close">×</button>' + jqXHR.responseText + '</div>');
                $('.alert .close').on("click", function (e) {
                    $(this).parent().fadeTo(500, 0).slideUp(500);
                });

            }
        });
    }
});


$("#cameraEditSettingsform").validate({
    submitHandler: function (form) {
        $('.form-process').fadeIn();
        var settingsInfo = {
            camId: $("#editCameraId").val(),
            vidcamid: $("#editCameraTag").val(),
            vidcamip: $("#editCameraIp").val(),
            vidcamport: $("#editCameraPort").val(),
            vidcamprotocol: $("#editCameraProtocol").val(),
            vidcamstreampath: $("#editCameraStreamPath").val(),
            vidcamuserid: $("#editCameraUserid").val(),
            vidcampwd: $("#editCameraPassword").val()
        };
        $.ajax({
            url: "api/updateCameraInfo",
            data: JSON.stringify(settingsInfo),
            type: 'POST',
            contentType: 'application/json',
            success: function (data, textStatus, jqXHR) {
                $("#editDialog").dialog('close');
                $("#cameraSettingsFeedback").html('<div class="alert alert-success offset4 span4"><button type="button" class="close">×</button>' + jqXHR.responseText + '</div>');
                $('.alert .close').on("click", function (e) {
                    $(this).parent().fadeTo(500, 0).slideUp(500);
                });
                getSavedCameras();

            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR.responseText);
                $("#editDialog").dialog('close');
                //document.getElementById("taxqueryError").innerHTML = jqXHR.responseText;
                //$('#settingsFeedback').html('<h5 class="error-msg" style:"align-center">' + jqXHR.responseText + '<h5>');
                $("#cameraSettingsFeedback").html('<div class="alert alert-warning offset4 span4"><button type="button" class="close">×</button>' + jqXHR.responseText + '</div>');
                $('.alert .close').on("click", function (e) {
                    $(this).parent().fadeTo(500, 0).slideUp(500);
                });
            }
        });
    }
});



function getSavedCameras() {
    var camList;
    if (cameraTable !== undefined) {
        $('#cameraTable').DataTable().clear();
        $('#cameraTable').DataTable().destroy();
    }

    $.getJSON('api/getCameraInfo', function (data) {
        var sdata = data;
        if (sdata.settingsErrorMsg === null) {
            camList = sdata.caminfo;
            cameraTable = $('#cameraTable').DataTable({
                'responsive': true,
                'bLengthChange': false,
                "pageLength": 10,
                "scrollCollapse": true,
                "paging": true,
                'data': camList,
                'columns': [
                    {"data": "updateTime"},
                    {"data": "camId"},
                    {"data": "vidcamid"},
                    {"data": "vidcamip"},
                    {"data": "vidcamport"},
                    {"data": "vidcamprotocol"},
                    {"data": "vidcamstreampath"},
                    {"data": "frstatus"},
                    {
                        data: null,
                        className: "center",
                        defaultContent: '<a class = "editor_edit" href="#"><i class="fa fa-edit" style="font-size:18px"></i></a>'
                    },
                    {
                        data: null,
                        className: "center",
                        defaultContent: '<a class="editor_remove" href="#"><i class="fa fa-trash" style="font-size:18px"></i> </a>'
                    }


                ],
                'order': [[0, 'asc']],
                'columnDefs': [
                    {className: "details-control", "targets": [0]},
                    {'render': function (data, type, row) {
                            return getEventTime(data);
                        }, 'targets': 0},
                    {'render': function (data, type, row) {
                            return getFrStatus(data, row['camId']);
                        }, 'targets': 7},
                ]
            });

            // Add event listener for opening and closing details
            $('#cameraTable tbody').on('click', 'td.details-control', function () {
                var tr = $(this).closest('tr');
                var row = cameraTable.row(tr);

                if (row.child.isShown()) {
                    // This row is already open - close it
                    row.child.hide();
                    tr.removeClass('shown');
                } else {
                    // Open this row
                    row.child(format(row.data())).show();
                    tr.addClass('shown');
                }
            });

            $('.editor_edit').click(function () {
                var fields = data.caminfo[$(this).closest('tr').index()];
                var fieldValues = [];
                fieldValues.push(fields.camId);
                fieldValues.push(fields.vidcamid);
                fieldValues.push(fields.vidcamip);
                fieldValues.push(fields.vidcamport);
                fieldValues.push(fields.vidcamprotocol);
                fieldValues.push(fields.vidcamstreampath);
                fieldValues.push(fields.vidcamuserid);
                fieldValues.push(fields.vidcampwd);
                $('#editDialog input[type="text"]').each(function (index, elem) {
                    $(elem).val(fieldValues[index]);
                });
                $('#editDialog').dialog({
                    title: 'Edit Camera Info',
                    width: '600',
                    modal: true

                });
            })

            $('.editor_remove').click(function () {
                var fields = data.caminfo[$(this).closest('tr').index()];
                console.log(fields.vidcamid);
                $('#recVal').text(fields.vidcamid);
                //$('#deleteDialog').dialog({title: 'Delete', width: '650'});

                $('#deleteDialog').dialog({
                    title: 'Delete',
                    resizable: false,
                    height: "auto",
                    width: 450,
                    modal: true,
                    buttons: {
                        "Delete": function () {
                            deleteCamera(fields.camId);
                        }
                    }
                });
            })

        }
    });


    /* Formatting function for row details - modify as you need */
    function format(d) {
        // `d` is the original data object for the row
        return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">' +
                '<tr>' +
                '<td align="left"> Status </td>' +
                '</table>';
    }

} // getSavedCamera()



function deleteCamera(camId) {

    var camInfo = {
        camId: camId,
        vidcamid: "",
        vidcamip: "",
        vidcamport: "",
        vidcamprotocol: "",
        vidcamstreampath: "",
        vidcamuserid: "",
        vidcampwd: ""
    };

    $.ajax({
        url: "api/delCamera",
        data: JSON.stringify(camInfo),
        type: 'POST',
        contentType: 'application/json',
        success: function (data, textStatus, jqXHR) {

            $("#deleteDialog").dialog('close');
            $("#cameraSettingsFeedback").html('<div class="alert alert-success offset4 span4"><button type="button" class="close">×</button>' + jqXHR.responseText + '</div>');
            $('.alert .close').on("click", function (e) {
                $(this).parent().fadeTo(500, 0).slideUp(500);
            });
            getSavedCameras();

        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR.responseText);

            $("#deleteDialog").dialog('close');
            $("#cameraSettingsFeedback").html('<div class="alert alert-warning offset4 span4"><button type="button" class="close">×</button>' + jqXHR.responseText + '</div>');
            $('.alert .close').on("click", function (e) {
                $(this).parent().fadeTo(500, 0).slideUp(500);
            });
        }
    });



}

function getEventTime(approvalTime) {
    var timeStr = "";
    if (approvalTime !== "")
        var timeStr = moment(approvalTime).format("DD MMM YYYY, hh:mm:ss A");
    return timeStr;
}


function getFrStatus(frstatus, camId) {
    var frVal = 'FR Stopped';
    switch (frstatus) {
        case 'Stopped':
            frVal = 'FR Stopped';
            break;

        case 'Started':
            frVal = 'FR Enabled';
            break;

        case undefined:
        case null:
            frstatus = 'Stopped';
            frVal = 'FR Stopped';
            break;
    }


    var str = '<div class="btn-group" style="width:130px;">'
            + '<button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="width:130px;">'
            + '<span class="icocr ico-' + frstatus + '"></span>' + frVal + ' <span class="caret"></span>'
            + '</button>'
            + '<ul class="dropdown-menu">'
            + '<li><a href="javascript:void(0)" onclick="updateFrStatus(\'' + camId + '\',\'Stopped\')"><span class="icocr ico-Stopped"></span>Stop FR</a></li>'
            + '<li><a href="javascript:void(0)" onclick="updateFrStatus(\'' + camId + '\',\'Started\')"><span class="icocr ico-Started"></span>Start FR</a></li>'
            + '</ul>'
            + '</div>';
    return str;


}


function updateFrStatus(camId, status) {

    switch (status) {
        case 'Stopped':
            stopFSClient(camId, status);
            break;

        case 'Started':
            startFSClient(camId, status);
            break;
    }

}


function startFSClient(camId, status) {

    var camInfo = {
        camId: camId,
        vidcamid: "",
        vidcamip: "",
        vidcamport: "",
        vidcamprotocol: "",
        vidcamstreampath: "",
        vidcamuserid: "",
        vidcampwd: "",
        frstatus: status
    };


    $.ajax({
        url: "api/startFSClient",
        data: JSON.stringify(camInfo),
        type: 'POST',
        contentType: 'application/json',
        success: function (data, textStatus, jqXHR) {
            $("#cameraSettingsFeedback").html('<div class="alert alert-success offset4 span4"><button type="button" class="close">x</button>' + jqXHR.responseText + '</div>');

            $('.alert .close').on("click", function (e) {
                $(this).parent().fadeTo(500, 0).slideUp(500);
            });
            getSavedCameras();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log('error : ' + errorThrown);
            console.log('textStatus: ' + textStatus);
            console.log('jqXHR: ' + jqXHR);
            console.log(jqXHR.responseText);
            $("#cameraSettingsFeedback").html('<div class="alert alert-warning offset4 span4"><button type="button" class="close">×</button>' + jqXHR.responseText + '</div>');
            $('.alert .close').on("click", function (e) {
                $(this).parent().fadeTo(500, 0).slideUp(500);
            });
        }
    });
}



function stopFSClient(camId, status) {

    var camInfo = {
        camId: camId,
        vidcamid: "",
        vidcamip: "",
        vidcamport: "",
        vidcamprotocol: "",
        vidcamstreampath: "",
        vidcamuserid: "",
        vidcampwd: "",
        frstatus: status
    };


    $.ajax({
        url: "api/stopFSClient",
        data: JSON.stringify(camInfo),
        type: 'POST',
        contentType: 'application/json',
        success: function (data, textStatus, jqXHR) {
            $("#cameraSettingsFeedback").html('<div class="alert alert-success offset4 span4"><button type="button" class="close">x</button>' + jqXHR.responseText + '</div>');

            $('.alert .close').on("click", function (e) {
                $(this).parent().fadeTo(500, 0).slideUp(500);
            });
            getSavedCameras();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log('error : ' + errorThrown);
            console.log('textStatus: ' + textStatus);
            console.log('jqXHR: ' + jqXHR);
            console.log(jqXHR.responseText);
            $("#cameraSettingsFeedback").html('<div class="alert alert-warning offset4 span4"><button type="button" class="close">×</button>' + jqXHR.responseText + '</div>');
            $('.alert .close').on("click", function (e) {
                $(this).parent().fadeTo(500, 0).slideUp(500);
            });
        }
    });
}

