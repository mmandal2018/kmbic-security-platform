/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kmbic.kmbicsecurityplatform.domain;

import org.jongo.marshall.jackson.oid.MongoId;
import org.jongo.marshall.jackson.oid.MongoObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document(collection = "serverSettings")
public class ServerSetting {

    @MongoId
    @MongoObjectId
    String id;

    Date updateTime;
    private String name;
    private String email;

    private String serverip;
    private String serverport;
    private String serverendpoint;
    private String deffsclientdir;
    private String fsListId;
    private String fsToken;
    private String settingsErrorMsg;

    public ServerSetting(){};

    public ServerSetting(String id, String name, String email, String serverip, String serverport) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.serverip = serverip;
        this.serverport = serverport;
    }

    public ServerSetting(String id, String name, String email, String serverip, String serverport, String serverendpoint, String deffsclientdir, String fsListId, String fsToken, String settingsErrorMsg) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.serverip = serverip;
        this.serverport = serverport;
        this.serverendpoint = serverendpoint;
        this.deffsclientdir = deffsclientdir;
        this.fsListId = fsListId;
        this.fsToken = fsToken;
        this.settingsErrorMsg = settingsErrorMsg;
    }

    public String getFsToken() {
        return fsToken;
    }

    public void setFsToken(String fsToken) {
        this.fsToken = fsToken;
    }

    public String getFsListId() {
        return fsListId;
    }

    public void setFsListId(String fsListId) {
        this.fsListId = fsListId;
    }

    public String getServerip() {
        return serverip;
    }

    public void setServerip(String serverip) {
        this.serverip = serverip;
    }

    public String getServerport() {
        return serverport;
    }

    public void setServerport(String serverport) {
        this.serverport = serverport;
    }

    public String getServerendpoint() {
        return serverendpoint;
    }

    public void setServerendpoint(String serverendpoint) {
        this.serverendpoint = serverendpoint;
    }

    public String getDeffsclientdir() {
        return deffsclientdir;
    }

    public void setDeffsclientdir(String deffsclientdir) {
        this.deffsclientdir = deffsclientdir;
    }

    public String getSettingsErrorMsg() {
        return settingsErrorMsg;
    }

    public void setSettingsErrorMsg(String settingsErrorMsg) {
        this.settingsErrorMsg = settingsErrorMsg;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "ServerSetting{" +
                "name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", serverip='" + serverip + '\'' +
                ", serverport='" + serverport + '\'' +
                ", serverendpoint='" + serverendpoint + '\'' +
                ", deffsclientdir='" + deffsclientdir + '\'' +
                ", fsListId='" + fsListId + '\'' +
                ", fsToken='" + fsToken + '\'' +
                '}';
    }
}
