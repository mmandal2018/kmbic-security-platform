/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kmbic.kmbicsecurityplatform.services;

//import com.kmbic.admin.repository.dao.impl.AccountInfoDao;
//import com.kmbic.admin.repository.dao.impl.EnrolledUserDao;
//import com.kmbic.admin.repository.dao.impl.ServerSettingsDao;
//import com.kmbic.admin.representations.*;
//import com.kmbic.admin.utils.HttpsTrustManager;
//import com.sun.jna.Pointer;
//import com.sun.jna.platform.win32.Kernel32;
//import com.sun.jna.platform.win32.WinNT;
//import org.apache.commons.io.FileUtils;

import com.kmbic.kmbicsecurityplatform.domain.RegisteredAccount;
import com.kmbic.kmbicsecurityplatform.domain.ServerSetting;
import com.kmbic.kmbicsecurityplatform.repository.AccountInfoRepository;
import com.kmbic.kmbicsecurityplatform.repository.ServerSettingRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

//import org.codehaus.jettison.json.JSONArray;
//import org.codehaus.jettison.json.JSONException;
//import org.codehaus.jettison.json.JSONObject;
//import org.imgscalr.Scalr;

//import static org.apache.commons.io.CopyUtils.copy;

/**
 *
 * @author kmbic
 */
@Service
public class SettingManagementServiceImpl implements SettingManagementService{

    @Autowired
    private ServerSettingRepository serverSettingRepository;

    @Autowired
    private AccountInfoRepository accountInfoRepository;

    private static final Logger LOGGER = LoggerFactory.getLogger(SettingManagementServiceImpl.class);


    @Autowired
    public SettingManagementServiceImpl(ServerSettingRepository repository) {
        this.serverSettingRepository = repository;
    }

    @Override
    public ServerSetting create(ServerSetting serverSetting) {
        LOGGER.info("Creating a new serverSetting entry with information: {}", serverSetting);

        ServerSetting persisted = serverSettingRepository.save(serverSetting);
        LOGGER.info("Created a new todo entry with information: {}", persisted);

        return persisted;
    }

    public RegisteredAccount getAccountInfo(String emailAddress) {
        RegisteredAccount account = accountInfoRepository.getAccountInfobyEmail(emailAddress);
        return account;
    }

    public ServerSetting getServerSetting(RegisteredAccount accountLogin) {

//        ServerSetting settinginfo = serverSettingRepository.getServerSettings(accountLogin);
        ServerSetting settinginfo = serverSettingRepository.findFirstByEmail("john@test.com");
//        System.out.println("settingInfo: " + settinginfo.getEmail());
        LOGGER.info("Getting the server setting entry with information: {}" + settinginfo);
        return settinginfo;

    }

//    public long startFSClient(ServerSetting serverSettings, CameraInfo cameraInfo) throws IOException, InterruptedException {
//
//        long pid = -1;
//        String fsPath = serverSettings.getDeffsclientdir();
//        File fsDir = new File(fsPath);
//        String fsExe = "FaceStream2.exe";
//
//        File f = new File(fsPath + File.separator + fsExe);
//        if (!f.exists()) {
//            log.error("FS client.exe not found");
//            return -1;
//        }
//
//        String configFileDir = fsPath + File.separator + "data";
//        String camName = cameraInfo.getVidcamid().replaceAll("\\s+", "");
//
//        File configFile = new File(configFileDir + File.separator + fsConfigFileName + "." + camName);
//
//        if (!configFile.exists()) {
//            log.error("FS Config File not found");
//            return -1;
//        }
//
//        String cmd = fsPath + "\\FaceStream2.exe -Src " + cameraInfo.getVidcamprotocol() + "://" + cameraInfo.getVidcamuserid() + ":" + cameraInfo.getVidcampwd()
//                + "@" + cameraInfo.getVidcamip() + ":" + cameraInfo.getVidcamport() + "/" + cameraInfo.getVidcamstreampath()
//                + " -Dst https://" + serverSettings.getServerip() + ":" + serverSettings.getServerport() + serverSettings.getServerendpoint()
//                + " -Cp " + configFileDir + File.separator + fsConfigFileName + "." + camName;
//
//        Process p = null;
//
//        try {
//            p = Runtime.getRuntime().exec(cmd, null, fsDir);
//            Thread.sleep(3000);
//            p.getInputStream().close();
//            p.getOutputStream().close();
//            p.getErrorStream().close();
//
//            // ReadStream s1 = new ReadStream("stdin", p.getInputStream());
//            //ReadStream s2 = new ReadStream("stderr", p.getErrorStream());
//            //s1.start();
//            //s2.start();
//            //p.waitFor();
//            pid = getProcessID(p);
//            log.info("Process Id is" + pid);
//        } catch (IOException e) {
//            log.error("Exception starting FSClient for cameraId " + cameraInfo.getVidcamid() + " " + e);
//            return pid;
//        }
//
//        return pid;
//
//    }
//
//    public boolean updateCamInfo(String emailAddress, CameraInfo caminfo) {
//        boolean saveResult = accountInfoDao.updateCameraInfo(emailAddress, caminfo);
//        return saveResult;
//    }
//
//    public boolean deleteCamera(String emailAddress, CameraInfo caminfo) {
//        boolean delResult = accountInfoDao.deleteCamera(emailAddress, caminfo);
//        return delResult;
//
//    }
//
//    public boolean updateCameraFRandPIDStatus(String emailAddress, CameraInfo caminfo) {
//        boolean saveResult = accountInfoDao.updateCameraFRandPIDStatus(emailAddress, caminfo);
//        return saveResult;
//
//    }
//
//    public CameraInfo getCameraInfobyID(String email, int camId) {
//        CameraInfo camInfo = accountInfoDao.getCameraInfobyID(email, camId);
//        return camInfo;
//    }
//
//    private long getProcessID(Process p) {
//        long pid = -1;
//        try {
//            //for windows
//            if (p.getClass().getName().equals("java.lang.Win32Process")
//                    || p.getClass().getName().equals("java.lang.ProcessImpl")) {
//                Field f = p.getClass().getDeclaredField("handle");
//                f.setAccessible(true);
//                long handl = f.getLong(p);
//                Kernel32 kernel = Kernel32.INSTANCE;
//                WinNT.HANDLE hand = new WinNT.HANDLE();
//                hand.setPointer(Pointer.createConstant(handl));
//                pid = kernel.GetProcessId(hand);
//                f.setAccessible(false);
//            } //for unix based operating systems
//            else if (p.getClass().getName().equals("java.lang.UNIXProcess")) {
//                Field f = p.getClass().getDeclaredField("pid");
//                f.setAccessible(true);
//                pid = f.getLong(p);
//                f.setAccessible(false);
//            }
//        } catch (IllegalAccessException | IllegalArgumentException | NoSuchFieldException | SecurityException ex) {
//            pid = -1;
//        }
//        return pid;
//
//    }
//
//    public boolean updateClientPID(String email, CameraInfo camInfo) {
//        boolean saveResult = accountInfoDao.updateClientPID(email, camInfo);
//        return saveResult;
//
//    }
//
//    public boolean stopFSClient(CameraInfo camInfo) throws InterruptedException {
//
//        String cmd = "cmd.exe /C taskkill.exe /f /pid " + camInfo.getClientpid();
//
//        Process p = null;
//        try {
//            p = Runtime.getRuntime().exec(cmd);
//            ReadStream s1 = new ReadStream("stdin", p.getInputStream());
//            ReadStream s2 = new ReadStream("stderr", p.getErrorStream());
//            s1.start();
//            s2.start();
//            int exitVal = p.waitFor();
//            if (exitVal == 0) {
//                return true;
//            } else {
//                return false;
//            }
//        } catch (IOException e) {
//            log.error("Exception stopping FSClient for cameraId " + camInfo.getVidcamid() + " " + e);
//            return false;
//        }
//
//    }
//
//    public boolean createFsConfigFile(String fsClientPath, CameraInfo caminfo) throws IOException, JSONException {
//
//        String baseDir = fsClientPath + File.separator + "data";
//        String camName = caminfo.getVidcamid().replaceAll("\\s+", "");
//
//        File sourceFile = new File(baseDir + File.separator + fsConfigFileName);
//
//        if (!sourceFile.exists()) {
//            log.error("FS Config File not found");
//            return false;
//        }
//
//        File fsConfigFile = new File(baseDir + File.separator + fsConfigFileName + "." + camName);
//
//        /*try {
//            FileUtils.copyFile(sourceFile, fsConfigFile);
//        } catch (IOException ex) {
//            log.error("Error creating the FSConfig file " + baseDir + File.separator + fsConfigFileName + "-" + caminfo.getVidcamid());
//            return false;
//        }*/
//        String jsonString = FileUtils.readFileToString(sourceFile);
//        JSONObject jObject = new JSONObject(jsonString);
//        JSONObject obj1 = jObject.getJSONObject("sending");
//        JSONObject obj2 = obj1.getJSONObject("camera-id");
//        obj2.put("value", caminfo.getVidcamid().trim());
//
//        String prettyJsonString = jObject.toString(2);
//        FileUtils.writeStringToFile(fsConfigFile, prettyJsonString);
//
//        return true;
//    }
//
//    public XResponse addNewUser(UserInfo userInfo, RegisteredAccount myAccount) throws UnsupportedEncodingException, IOException, MalformedURLException, JSONException, KeyManagementException {
//
//        userInfo.setAccountemail(myAccount.getEmail());
//        XResponse xresponse = new XResponse();
//
//        //First get the serversettings to figure out baseURL
//        ServerSetting serverSettings = serverSettingsDao.getServerSettings(myAccount);
//        String fslistId = serverSettings.getFsListId();
//
//        if (serverSettings == null) {
//            log.error("No server Settings found for Account " + myAccount.getEmail());
//            xresponse.setErrorCode(1);
//            xresponse.setErrorMsg("No server Settings found for Account " + myAccount.getEmail());
//            return xresponse;
//        }
//
//        String baseUrl = "https://" + serverSettings.getServerip() + ":" + serverSettings.getServerport() + "/3";
//        //First add the user using Luna APIs and then save the information locally as well.
//        String personName = userInfo.getFirstName() + " " + userInfo.getLastName();
//
//        String ApiResponse = createNewPersonwithDescriptor(baseUrl, personName);
//        if (!ApiResponse.equals("API_ERROR")) {
//            JSONObject ApiResult = new JSONObject(ApiResponse);
//            String person_id = ApiResult.getString("person_id");
//            String ApiResponse1 = extractImageDescriptor(baseUrl, userInfo.getPhotoList().get(0));
//
//            if (!ApiResponse1.equals("API_ERROR")) {
//                JSONObject obj = new JSONObject(ApiResponse1);
//                JSONArray faces = obj.getJSONArray("faces");
//                JSONObject obj1 = (JSONObject) faces.get(0);
//                String id = obj1.getString("id");
//
//                // Now Attach this descriptor id to the person_id
//                String ApiResponse2 = attachDescriptorIdtoPerson(baseUrl, person_id, id);
//
//                if (!ApiResponse2.equals("API_ERROR")) {
//                    // All good so far. Now create the thumbnailStr and save locally as well
//                    userInfo.setPerson_id(person_id);
//                    if ((userInfo.getPhotoList() != null) && (!userInfo.getPhotoList().isEmpty())) {
//                        userInfo.setThumbnail(createThumbnailString(userInfo.getPhotoList().get(0)));
//                    } else {
//                        userInfo.setThumbnail(unknown_user);
//                    }
//                    xresponse = userInfoDao.enrollUser(userInfo);
//
//                    // Now attach the person to the default facestream list id
//                    String ApiResponse3 = attachFSListIdtoPerson(baseUrl, person_id, fslistId);
//                    if (ApiResponse3.equals("API_ERROR")) {
//                        xresponse.setErrorCode(1);
//                        xresponse.setErrorMsg("Error attaching Facestream List to User on Luna Server");
//                    }
//
//                } else {
//                    xresponse.setErrorCode(1);
//                    xresponse.setErrorMsg("Error attaching descriptor to User on Luna Server");
//                }
//            } else {
//                xresponse.setErrorCode(1);
//                xresponse.setErrorMsg("Error extracting image descriptor for User " + personName);
//            }
//        } else {
//            xresponse.setErrorCode(1);
//            xresponse.setErrorMsg("Error creating new User " + personName);
//        }
//        return xresponse;
//    }
//
//    public String createThumbnailString(String imgStr) throws UnsupportedEncodingException, IOException {
//
//        try {
//            byte[] imageByte = Base64.getDecoder().decode(new String(imgStr).getBytes("UTF-8"));
//            InputStream input = new ByteArrayInputStream(imageByte);
//            BufferedImage profileImg = ImageIO.read(input);
//            BufferedImage thumbnail = Scalr.resize(profileImg, Scalr.Method.QUALITY, 200);
//            ByteArrayOutputStream baos = new ByteArrayOutputStream();
//            ImageIO.write(thumbnail, "jpeg", baos);
//            baos.flush();
//            byte[] imageInByte = baos.toByteArray();
//            baos.close();
//            String thumbnailStr = Base64.getEncoder().encodeToString(imageInByte);
//            return thumbnailStr;
//        } catch (Exception ex) {
//            return "";
//        }
//
//    }
//
//    public List<UserTable> getAllUsers(RegisteredAccount myAccount) {
//        List<UserTable> userList = userInfoDao.getAllUsers(myAccount);
//        return userList;
//    }

//    public static String createNewPersonwithDescriptor(String baseUrl, String personName) throws MalformedURLException, JSONException, KeyManagementException {
//
//        HttpsTrustManager.allowAllSSL();
//        String api1 = baseUrl + "/storage/persons";
//        URL object = new URL(api1);
//        String lunaResponse = "";
//
//        try {
//            HttpURLConnection con = (HttpURLConnection) object.openConnection();
//            con.setDoOutput(true);
//            con.setDoInput(true);
//            String basicAuth = "Basic ZGVtb0BkZW1vLmRlbW86REVNT19URVNU";
//            con.setRequestProperty("Authorization", basicAuth);
//            con.setRequestProperty("Content-Type", "application/json");
//            //con.setRequestProperty("Accept", "application/json");
//            con.setRequestMethod("POST");
//
//            JSONObject request = new JSONObject();
//            request.put("user_data", personName);
//            OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());
//            wr.write(request.toString());
//            wr.flush();
//
//            //display what returns the POST request
//            StringBuilder sb = new StringBuilder();
//            int HttpResult = con.getResponseCode();
//            if (HttpResult == HttpURLConnection.HTTP_CREATED) {
//                BufferedReader br = new BufferedReader(
//                        new InputStreamReader(con.getInputStream(), "utf-8"));
//                String line = null;
//                while ((line = br.readLine()) != null) {
//                    sb.append(line + "\n");
//                }
//                br.close();
//                //System.out.println("" + sb.toString());
//                lunaResponse = sb.toString();
//            } else {
//                lunaResponse = "API_ERROR";
//            }
//
//        } catch (IOException e) {
//            return "API_ERROR";
//        }
//
//        return lunaResponse;
//    }
//
//    private String extractImageDescriptor(String baseUrl, String imgStr) throws MalformedURLException, KeyManagementException {
//
//        HttpsTrustManager.allowAllSSL();
//        String api2 = baseUrl + "/storage/descriptors";
//        URL object = new URL(api2);
//        String lunaResponse = "";
//
//        try {
//            HttpURLConnection con = (HttpURLConnection) object.openConnection();
//            con.setDoOutput(true);
//            con.setDoInput(true);
//            String basicAuth = "Basic ZGVtb0BkZW1vLmRlbW86REVNT19URVNU";
//            con.setRequestProperty("Authorization", basicAuth);
//            con.setRequestProperty("Content-Type", "image/png");
//            //con.setRequestProperty("Accept", "application/json");
//            con.setRequestMethod("POST");
//
//            byte[] imageByte = Base64.getDecoder().decode(new String(imgStr).getBytes("UTF-8"));
//            InputStream in = new ByteArrayInputStream(imageByte);
//
//            OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());
//            copy(in, con.getOutputStream());
//            wr.flush();
//            wr.close();
//
//            //display what returns the POST request
//            StringBuilder sb = new StringBuilder();
//            int HttpResult = con.getResponseCode();
//            if (HttpResult == HttpURLConnection.HTTP_CREATED) {
//                BufferedReader br = new BufferedReader(
//                        new InputStreamReader(con.getInputStream(), "utf-8"));
//                String line = null;
//                while ((line = br.readLine()) != null) {
//                    sb.append(line + "\n");
//                }
//                br.close();
//                //System.out.println("" + sb.toString());
//                lunaResponse = sb.toString();
//            } else {
//                lunaResponse = "API_ERROR";
//            }
//
//        } catch (IOException e) {
//            return "API_ERROR";
//        }
//
//        return lunaResponse;
//    }
//
//    private String attachDescriptorIdtoPerson(String baseUrl, String person_id, String id) throws MalformedURLException, KeyManagementException {
//
//        HttpsTrustManager.allowAllSSL();
//        String api1 = baseUrl + "/storage/persons/" + person_id + "/linked_descriptors?descriptor_id=" + id + "&do=attach";
//        URL object = new URL(api1);
//        String lunaResponse = "";
//
//        try {
//            Field methodsField = HttpURLConnection.class.getDeclaredField("methods");
//            methodsField.setAccessible(true);
//            // get the methods field modifiers
//            Field modifiersField = Field.class.getDeclaredField("modifiers");
//            // bypass the "private" modifier
//            modifiersField.setAccessible(true);
//
//            // remove the "final" modifier
//            modifiersField.setInt(methodsField, methodsField.getModifiers() & ~Modifier.FINAL);
//
//            /* valid HTTP methods */
//            String[] methods = {
//                "GET", "POST", "HEAD", "OPTIONS", "PUT", "DELETE", "TRACE", "PATCH"
//            };
//            // set the new methods - including patch
//            methodsField.set(null, methods);
//
//        } catch (SecurityException | IllegalArgumentException | IllegalAccessException | NoSuchFieldException e) {
//            return "API_ERROR";
//        }
//
//        try {
//            HttpURLConnection con = (HttpURLConnection) object.openConnection();
//            con.setDoOutput(true);
//            con.setDoInput(true);
//            String basicAuth = "Basic ZGVtb0BkZW1vLmRlbW86REVNT19URVNU";
//            con.setRequestProperty("Authorization", basicAuth);
//            //con.setRequestProperty("Content-Type", "application/json");
//            //con.setRequestProperty("Accept", "application/json");
//            con.setRequestMethod("PATCH");
//
//            //display what returns the POST request
//            StringBuilder sb = new StringBuilder();
//            int HttpResult = con.getResponseCode();
//            if (HttpResult == HttpURLConnection.HTTP_NO_CONTENT) {
//                BufferedReader br = new BufferedReader(
//                        new InputStreamReader(con.getInputStream(), "utf-8"));
//                String line = null;
//                while ((line = br.readLine()) != null) {
//                    sb.append(line + "\n");
//                }
//                br.close();
//                //System.out.println("" + sb.toString());
//                lunaResponse = sb.toString();
//            } else {
//                lunaResponse = "API_ERROR";
//            }
//
//        } catch (IOException e) {
//            return "API_ERROR";
//        }
//
//        return lunaResponse;
//    }
//
//    public XResponse delUser(String personId, RegisteredAccount myAccount) throws MalformedURLException, KeyManagementException {
//
//        XResponse xresponse = new XResponse();
//
//        //First get the serversettings to figure out baseURL
//        ServerSetting serverSettings = serverSettingsDao.getServerSettings(myAccount);
//
//        if (serverSettings == null) {
//            log.error("No server Settings found for Account " + myAccount.getEmail());
//            xresponse.setErrorCode(1);
//            xresponse.setErrorMsg("No server Settings found for Account " + myAccount.getEmail());
//            return xresponse;
//        }
//
//        String baseUrl = "https://" + serverSettings.getServerip() + ":" + serverSettings.getServerport() + "/3";
//        String ApiResponse = delPersonWithDescriptorId(baseUrl, personId);
//        if (ApiResponse.equals("API_ERROR")) {
//
//            xresponse.setErrorCode(1);
//            xresponse.setErrorMsg("Error Deleting User " + personId + "  from Luna Server");
//        } else {
//            xresponse = userInfoDao.deleteUser(personId);
//        }
//
//        return xresponse;
//    }
//
//    private String delPersonWithDescriptorId(String baseUrl, String person_id) throws MalformedURLException, KeyManagementException {
//
//        HttpsTrustManager.allowAllSSL();
//        String api1 = baseUrl + "/storage/persons/" + person_id;
//        URL object = new URL(api1);
//        String lunaResponse = "";
//
//        try {
//            Field methodsField = HttpURLConnection.class.getDeclaredField("methods");
//            methodsField.setAccessible(true);
//            // get the methods field modifiers
//            Field modifiersField = Field.class.getDeclaredField("modifiers");
//            // bypass the "private" modifier
//            modifiersField.setAccessible(true);
//
//            // remove the "final" modifier
//            modifiersField.setInt(methodsField, methodsField.getModifiers() & ~Modifier.FINAL);
//
//            /* valid HTTP methods */
//            String[] methods = {
//                "GET", "POST", "HEAD", "OPTIONS", "PUT", "DELETE", "TRACE", "PATCH"
//            };
//            // set the new methods - including patch
//            methodsField.set(null, methods);
//
//        } catch (SecurityException | IllegalArgumentException | IllegalAccessException | NoSuchFieldException e) {
//            return "API_ERROR";
//        }
//
//        try {
//            HttpURLConnection con = (HttpURLConnection) object.openConnection();
//            con.setDoOutput(true);
//            con.setDoInput(true);
//            String basicAuth = "Basic ZGVtb0BkZW1vLmRlbW86REVNT19URVNU";
//            con.setRequestProperty("Authorization", basicAuth);
//            //con.setRequestProperty("Content-Type", "application/json");
//            //con.setRequestProperty("Accept", "application/json");
//            con.setRequestMethod("DELETE");
//
//            //display what returns the POST request
//            StringBuilder sb = new StringBuilder();
//            int HttpResult = con.getResponseCode();
//            if (HttpResult == HttpURLConnection.HTTP_NO_CONTENT) {
//                BufferedReader br = new BufferedReader(
//                        new InputStreamReader(con.getInputStream(), "utf-8"));
//                String line = null;
//                while ((line = br.readLine()) != null) {
//                    sb.append(line + "\n");
//                }
//                br.close();
//                //System.out.println("" + sb.toString());
//                lunaResponse = sb.toString();
//            } else {
//                lunaResponse = "API_ERROR";
//            }
//
//        } catch (IOException e) {
//            return "API_ERROR";
//        }
//
//        return lunaResponse;
//    }
//
//    private String attachFSListIdtoPerson(String baseUrl, String person_id, String fslistId) throws KeyManagementException, MalformedURLException {
//        HttpsTrustManager.allowAllSSL();
//        String api = baseUrl + "/storage/persons/" + person_id + "/linked_lists?list_id=" + fslistId + "&do=attach";
//        URL object = new URL(api);
//        String lunaResponse = "";
//
//        try {
//            Field methodsField = HttpURLConnection.class.getDeclaredField("methods");
//            methodsField.setAccessible(true);
//            // get the methods field modifiers
//            Field modifiersField = Field.class.getDeclaredField("modifiers");
//            // bypass the "private" modifier
//            modifiersField.setAccessible(true);
//
//            // remove the "final" modifier
//            modifiersField.setInt(methodsField, methodsField.getModifiers() & ~Modifier.FINAL);
//
//            /* valid HTTP methods */
//            String[] methods = {
//                "GET", "POST", "HEAD", "OPTIONS", "PUT", "DELETE", "TRACE", "PATCH"
//            };
//            // set the new methods - including patch
//            methodsField.set(null, methods);
//
//        } catch (SecurityException | IllegalArgumentException | IllegalAccessException | NoSuchFieldException e) {
//            return "API_ERROR";
//        }
//
//        try {
//            HttpURLConnection con = (HttpURLConnection) object.openConnection();
//            con.setDoOutput(true);
//            con.setDoInput(true);
//            String basicAuth = "Basic ZGVtb0BkZW1vLmRlbW86REVNT19URVNU";
//            con.setRequestProperty("Authorization", basicAuth);
//            //con.setRequestProperty("Content-Type", "application/json");
//            //con.setRequestProperty("Accept", "application/json");
//            con.setRequestMethod("PATCH");
//
//            //display what returns the POST request
//            StringBuilder sb = new StringBuilder();
//            int HttpResult = con.getResponseCode();
//            if (HttpResult == HttpURLConnection.HTTP_NO_CONTENT) {
//                BufferedReader br = new BufferedReader(
//                        new InputStreamReader(con.getInputStream(), "utf-8"));
//                String line = null;
//                while ((line = br.readLine()) != null) {
//                    sb.append(line + "\n");
//                }
//                br.close();
//                //System.out.println("" + sb.toString());
//                lunaResponse = sb.toString();
//            } else {
//                lunaResponse = "API_ERROR";
//            }
//
//        } catch (IOException e) {
//            return "API_ERROR";
//        }
//
//        return lunaResponse;
//    }

}
