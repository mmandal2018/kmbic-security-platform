package com.kmbic.kmbicsecurityplatform.services;

import com.kmbic.kmbicsecurityplatform.domain.RegisteredAccount;
import com.kmbic.kmbicsecurityplatform.domain.ServerSetting;

public interface SettingManagementService {

    public ServerSetting create(ServerSetting serverSetting);

    RegisteredAccount getAccountInfo(String emailAddress);

    ServerSetting getServerSetting(RegisteredAccount accountLogin);


}
