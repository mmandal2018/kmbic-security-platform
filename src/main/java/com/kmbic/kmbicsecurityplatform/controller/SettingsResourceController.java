package com.kmbic.kmbicsecurityplatform.controller;


import com.kmbic.kmbicsecurityplatform.domain.RegisteredAccount;
import com.kmbic.kmbicsecurityplatform.domain.ServerSetting;
import com.kmbic.kmbicsecurityplatform.services.SettingManagementServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.logging.Logger;

@RestController
@RequestMapping("/api")
public class SettingsResourceController {

    private static Logger logger = Logger.getLogger(SettingsResourceController.class.getName());

    @Autowired
    private SettingManagementServiceImpl settingsManagementService;

    @RequestMapping(value = "/serverSetting", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON )
    public Response getServiceSetting(@Context HttpServletRequest request,
                                       @Context HttpServletResponse response){

        //This will have to be replaced by the actual login account from Spring Security
        RegisteredAccount myAccount = new RegisteredAccount();
        myAccount.setEmail("john@test.com");
        ServerSetting settings = settingsManagementService.getServerSetting(myAccount);
        if (settings != null) {
            return Response.status(Response.Status.OK).entity(settings).build();
        } else {
            settings = new ServerSetting();
            settings.setSettingsErrorMsg("Error getting the server settings data");
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(settings).build();
        }
    }

//

}


