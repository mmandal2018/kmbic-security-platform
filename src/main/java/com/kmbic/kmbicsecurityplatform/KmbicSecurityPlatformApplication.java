package com.kmbic.kmbicsecurityplatform;

import com.kmbic.kmbicsecurityplatform.domain.ServerSetting;
import com.kmbic.kmbicsecurityplatform.repository.ServerSettingRepository;
import com.mongodb.DB;
import com.mongodb.MongoClient;
import com.mongodb.MongoException;
import org.jongo.Jongo;
import org.jongo.MongoCollection;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import java.net.UnknownHostException;
import java.util.List;

@SpringBootApplication
public class KmbicSecurityPlatformApplication {

	public static void main(String[] args) {
		SpringApplication.run(KmbicSecurityPlatformApplication.class, args);
	}


	@Bean
	public Jongo jongo() {
		DB db;
		db = new MongoClient("127.0.0.1", 27017).getDB("securityplatform");
		return new Jongo(db);
	}

	@Bean
	public MongoCollection serverSettings() {
		return jongo().getCollection("serverSettings");
	}


	@Bean
	CommandLineRunner commandLineRunner(ServerSettingRepository serverSettingRepository) {
		return strings -> {
			serverSettingRepository.save(new ServerSetting("123","John",
					"john@test.com", "10.10.2.2", "8090",
					"/facestream/setting123", "~/clientDir", "list1, list2", "abc123", "test"));
		};
	}


}
