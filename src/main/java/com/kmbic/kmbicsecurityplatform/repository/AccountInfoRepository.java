/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kmbic.kmbicsecurityplatform.repository;

import com.kmbic.kmbicsecurityplatform.domain.RegisteredAccount;
import com.mongodb.WriteResult;
import org.apache.log4j.Logger;
import org.jongo.Jongo;
import org.jongo.MongoCollection;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 *
 */
@Repository
public class AccountInfoRepository extends BaseRepository {

    static Logger log = Logger.getLogger(AccountInfoRepository.class.getName());
    private MongoCollection accountInfo;

    public AccountInfoRepository(Jongo jongo) {
        super(jongo);
        this.accountInfo = jongo.getCollection(ACCOUNTS_COLLECTION);
    }

    protected MongoCollection getAccountsCollection() {
        if (accountInfo == null) {
            this.accountInfo = jongo.getCollection(ACCOUNTS_COLLECTION);
        }
        return accountInfo;

    }



    public RegisteredAccount getAccountInfobyEmail(String email) {
        RegisteredAccount accountinfo = null;
        try {
            MongoCollection accountColl = getAccountsCollection();
            accountinfo = accountColl.findOne("{email: # }", email).as(RegisteredAccount.class);
        } catch (Exception ex) {
            log.error("Account not Found for " + email + ex);
        }
        if (accountinfo == null) {
            log.error("Account not Found for " + email);
        }
        return accountinfo;
    }



}
