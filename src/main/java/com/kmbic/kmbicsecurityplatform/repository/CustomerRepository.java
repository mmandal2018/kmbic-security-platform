package com.kmbic.kmbicsecurityplatform.repository;

import com.kmbic.kmbicsecurityplatform.domain.Customer;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface CustomerRepository extends MongoRepository<Customer, Long> {

    Customer findByFirstName(String firstName);

    Customer findByLastName(String lastName);

    List<Customer> findAllByFirstName(String firstName);

}
