package com.kmbic.kmbicsecurityplatform.repository;

import org.apache.log4j.Logger;
import org.jongo.Jongo;
import org.jongo.MongoCollection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;

public class BaseRepository {

    @Autowired
    protected Jongo jongo;

    static final String JOBS_COLLECTION = "Jobs";
    static final String ADMIN_COLLECTION = "Admin_Users";
    static final String USERS_COLLECTION = "Enrolled_Users";
    static final String REPORTS_COLLECTION = "Access_Report";
    static final String SERVER_SETTINGS_COLLECTION = "serverSettings";
    static final String ACCOUNTS_COLLECTION = "Account_Settings";
    static final String CAMERA_COLLECTION = "Linked_Cameras";

    static Logger logger = Logger.getLogger(BaseRepository.class.getName());
    protected MongoCollection reportsCollection;

    public BaseRepository(Jongo jongo) {
        this.jongo = jongo;
    }

    public MongoCollection getCollection(String name) {
        return jongo.getCollection(name);
    }

    public MongoCollection getTransactionCollection() {
        if (reportsCollection == null) {
            reportsCollection = getCollection(REPORTS_COLLECTION);
        }
        return reportsCollection;
    }
}
