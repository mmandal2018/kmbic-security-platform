/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kmbic.kmbicsecurityplatform.repository;

import com.kmbic.kmbicsecurityplatform.domain.RegisteredAccount;
import com.kmbic.kmbicsecurityplatform.domain.ServerSetting;
import org.apache.log4j.Logger;
import org.jongo.Jongo;
import org.jongo.MongoCollection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 *
 *
 */
//@Repository
public interface ServerSettingRepository extends MongoRepository<ServerSetting, String> {

    ServerSetting findFirstByEmail(String email);

}


//        extends BaseRepository {
//
//    static Logger log = Logger.getLogger(ServerSettingRepository.class.getName());
//    private MongoCollection serverSettings;
//
//    public ServerSettingRepository(Jongo jongo) {
//        super(jongo);
//        this.serverSettings = jongo.getCollection(SERVER_SETTINGS_COLLECTION);
//    }
//
//    protected MongoCollection getServerSettingsCollection() {
//        if (serverSettings == null) {
//            this.serverSettings = jongo.getCollection(SERVER_SETTINGS_COLLECTION);
//        }
//        return serverSettings;
//
//    }
//
//    public ServerSetting getServerSettings(RegisteredAccount account) {
//        ServerSetting settingsinfo = null;
//        try {
//            MongoCollection settingsColl = getServerSettingsCollection();
//            settingsinfo = settingsColl.findOne("{email: #}", account.getEmail()).as(ServerSetting.class);
//        } catch (Exception ex) {
//            log.error("No settings found for " + account.getEmail() + ex);
//        }
//        if (settingsinfo == null) {
//            log.error("No settings found for " + account.getEmail());
//        }
//        return settingsinfo;
//    }
//
//}
