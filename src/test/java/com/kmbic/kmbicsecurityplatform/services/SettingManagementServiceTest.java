package com.kmbic.kmbicsecurityplatform.services;

import com.kmbic.kmbicsecurityplatform.domain.ServerSetting;
import com.kmbic.kmbicsecurityplatform.repository.ServerSettingRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.*;


@RunWith(MockitoJUnitRunner.class)
public class SettingManagementServiceTest {

    @Mock
    private ServerSettingRepository repository;

    private SettingManagementService service;

    @Before
    public void setUp() {
        this.service = new SettingManagementServiceImpl(repository);
    }


    @Test
    public void create_ShouldSaveNewServerSettingEntry() {
        ServerSetting serverSetting = new ServerSetting("111", "Moumita", "moumita@test.com", "10.22.11.0", "8000");

        when(repository.save(isA(ServerSetting.class))).thenAnswer(invocation -> (ServerSetting) invocation.getArguments()[0]);

        service.create(serverSetting);

        ArgumentCaptor<ServerSetting> savedServerSettingArgument = ArgumentCaptor.forClass(ServerSetting.class);

        verify(repository, times(1)).save(savedServerSettingArgument.capture());
        verifyNoMoreInteractions(repository);

        ServerSetting savedServerSetting = savedServerSettingArgument.getValue();
       Assert.assertNotNull(savedServerSetting);
        Assert.assertEquals(serverSetting, savedServerSetting);
    }

//    @Test
//    public void create_ShouldReturnTheInformationOfCreatedTodoEntry() {
//        TodoDTO newTodo = new TodoDTOBuilder()
//                .title(TITLE)
//                .description(DESCRIPTION)
//                .build();
//
//        when(repository.save(isA(Todo.class))).thenAnswer(invocation -> {
//            Todo persisted = (Todo) invocation.getArguments()[0];
//            ReflectionTestUtils.setField(persisted, "id", ID);
//            return persisted;
//        });
//
//        TodoDTO returned = service.create(newTodo);
//
//        assertThatTodoDTO(returned)
//                .hasId(ID)
//                .hasTitle(TITLE)
//                .hasDescription(DESCRIPTION);
//    }
//
//    @Test(expected = TodoNotFoundException.class)
//    public void delete_TodoEntryNotFound_ShouldThrowException() {
//        when(repository.findOne(ID)).thenReturn(Optional.empty());
//
//        service.findById(ID);
//    }

}
